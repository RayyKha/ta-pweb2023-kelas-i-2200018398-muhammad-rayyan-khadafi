<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exam Result</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f0f0f0;
        }
        .container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        .result {
            color: #333;
            font-size: 20px;
            margin: 10px 0;
            padding: 10px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            background-color: #f9f9f9;
        }
        .result.passed {
            color: #155724;
            background-color: #d4edda;
        }
        .result.failed {
            color: #721c24;
            background-color: #f8d7da;
        }
    </style>
</head>
<body>
   <div class="container">
        <?php
        $scores = array(80, 90, 70, 60, 50);
        foreach($scores as $nilai){
            if($nilai >= 60){
                echo "<p class='result passed'>Nilai Anda $nilai, Anda Lulus</p>";
            } else {
                echo "<p class='result failed'>Nilai Anda $nilai, Anda Tidak Lulus</p>";
            }
        }
        ?>
    </div>
</body>
</html>
