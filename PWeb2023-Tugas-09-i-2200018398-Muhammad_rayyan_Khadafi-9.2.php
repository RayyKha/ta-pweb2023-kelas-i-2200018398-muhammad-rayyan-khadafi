<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>9.2</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 20px;
        }
        
        h1 {
            color: #333;
        }
        
        .result {
            margin-top: 20px;
            background-color: #fff;
            padding: 10px;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <?php
    $a = 5;
    $b = 4;

    echo "<h1>Comparison Operators</h1>";
    echo "<div class='result'>$a == $b : " . ($a == $b) . "</div>";
    echo "<div class='result'>$a != $b : " . ($a != $b) . "</div>";
    echo "<div class='result'>$a > $b : " . ($a > $b) . "</div>";
    echo "<div class='result'>$a < $b : " . ($a < $b) . "</div>";
    echo "<div class='result'>($a == $b) && ($a > $b) : " . (($a == $b) && ($a > $b)) . "</div>";
    echo "<div class='result'>($a == $b) || ($a > $b) : " . (($a == $b) || ($a > $b)) . "</div>";
    ?>
</body>
</html>
