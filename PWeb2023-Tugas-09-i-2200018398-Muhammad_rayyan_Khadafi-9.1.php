<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>9.1</title>
    
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 20px;
        }
        
        h1 {
            color: #333;
        }
        
        .result {
            font-size: 18px;
            margin-top: 20px;
            background-color: #fff;
            padding: 10px;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <?php
    $gaji = 1000000;
    $pajak = 0.1;
    $thp  = $gaji - ($gaji*$pajak);

    echo "<h1>Gaji sebelum pajak = Rp. $gaji</h1>";
    echo "<div class='result'>Gaji yang dibawa pulang = Rp. $thp</div>";
    ?>
</body>
</html>

