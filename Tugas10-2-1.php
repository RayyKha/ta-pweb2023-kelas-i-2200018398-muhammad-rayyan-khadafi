<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nilai = $_POST['nilai'];

    if ($nilai >= 80.00 && $nilai <= 100.00) {
        $nilai_huruf = "A";
        $nilai_numerik = 4.00;
    } elseif ($nilai >= 76.25 && $nilai <= 79.99) {
        $nilai_huruf = "A-";
        $nilai_numerik = 3.67;
    } elseif ($nilai >= 68.75 && $nilai <= 76.24) {
        $nilai_huruf = "B+";
        $nilai_numerik = 3.33;
    } elseif ($nilai >= 65.00 && $nilai <= 68.74) {
        $nilai_huruf = "B";
        $nilai_numerik = 3.00;
    } elseif ($nilai >= 62.50 && $nilai <= 64.99) {
        $nilai_huruf = "B-";
        $nilai_numerik = 2.67;
    } elseif ($nilai >= 57.50 && $nilai <= 62.49) {
        $nilai_huruf = "C+";
        $nilai_numerik = 2.33;
    } elseif ($nilai >= 55.00 && $nilai <= 57.49) {
        $nilai_huruf = "C";
        $nilai_numerik = 2.00;
    } elseif ($nilai >= 51.25 && $nilai <= 54.99) {
        $nilai_huruf = "C-";
        $nilai_numerik = 1.67;
    } elseif ($nilai >= 43.75 && $nilai <= 51.24) {
        $nilai_huruf = "D+";
        $nilai_numerik = 1.33;
    } elseif ($nilai >= 40.00 && $nilai <= 43.74) {
        $nilai_huruf = "D";
        $nilai_numerik = 1.00;
    } elseif ($nilai >= 0.00 && $nilai <= 39.99) {
        $nilai_huruf = "E";
        $nilai_numerik = 0.00;
    } else {
        $nilai_huruf = "Nilai Tidak Valid";
        $nilai_numerik = "Nilai Tidak Valid";
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Penilaian Nilai</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 100vh;
            background-color: #f0f0f0;
            color: #333;
            transition: background-color 0.3s ease, color 0.3s ease;
        }

        h1 {
            text-align: center;
            color: #333;
        }

        .container {
            text-align: center;
            max-width: 600px;
            background-color: #fff;
            padding: 40px;
            border-radius: 10px;
            box-shadow: 0px 0px 10px rgba(0,0,0,0.1);
            transition: background-color 0.3s ease, box-shadow 0.3s ease;
        }

        form {
            margin-top: 20px;
        }

        label {
            display: block;
            margin-bottom: 10px;
            color: #333;
        }

        input[type="number"] {
            width: 50%;
            padding: 12px;
            border-radius: 5px;
            border: 1px solid #ddd;
            transition: border-color 0.3s ease;
        }

        input[type="number"]:focus {
            border-color: #007BFF;
        }

        input[type="submit"] {
            display: block;
            width: 100%;
            padding: 10px;
            border: none;
            color: #fff;
            background-color: #007BFF;
            border-radius: 5px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        input[type="submit"]:hover {
            background-color: #0056b3;
        }

        .result {
            margin-top: 20px;
            background-color: #eee;
            padding: 10px;
            border-radius: 5px;
        }

        .result p {
            margin: 0;
            color: #333;
        }
    </style>
</head>
<body>
    <div class="container">
        <h1>Penilaian Nilai</h1>
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <label for="nilai">Masukkan nilai angka:</label>
            <input type="number" id="nilai" name="nilai" required min="0" max="100">
            <input type="submit" value="Submit">
        </form>

        <?php if ($_SERVER["REQUEST_METHOD"] == "POST") { ?>
            <div class="result">
                <h2>Hasil Penilaian:</h2>
                <p>Nilai Angka: <?php echo $nilai; ?></p>
                <p>Nilai Huruf: <?php echo $nilai_huruf; ?></p>
                <p>Nilai Numerik: <?php echo $nilai_numerik; ?></p>
            </div>
        <?php } ?>
    </div>
</body>
</html>

