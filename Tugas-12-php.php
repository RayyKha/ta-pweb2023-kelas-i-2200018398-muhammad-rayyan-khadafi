<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas 12</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f5f5f5;
        }
        .container {
            width: 80%;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        h1 {
            text-align: center;
            color: #333;
            margin-bottom: 20px;
        }
        .result {
            color: #333;
            font-size: 20px;
            margin-bottom: 10px;
            padding: 10px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }
        .result.passed {
            color: #155724;
            background-color: #d4edda;
        }
        .result.failed {
            color: #721c24;
            background-color: #f8d7da;
        }
    </style>
</head>
<body>
   <div class="container">
        <h1>Penilaian ujian</h1>
        <?php
        function checkPassFail($nilai) {
            if ($nilai >= 60) {
                return "Lulus";
            } else {
                return "Tidak Lulus";
            }
        }

        $scores = array(80, 90, 70, 60, 50);
        foreach ($scores as $nilai) {
            $status = checkPassFail($nilai);
            if ($status == "Lulus") {
                echo "<p class='result passed'>Nilai Anda: $nilai<br>Anda $status</p>";
            } else {
                echo "<p class='result failed'>Nilai Anda: $nilai<br>Anda $status</p>";
            }
        }
        ?>
    </div>
</body>
</html>
