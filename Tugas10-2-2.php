<!DOCTYPE html>
<html>
<head>
    <title>Piramida Bintang</title>
</head>
<body>
    <h2>Form Tinggi Piramida</h2>
    <form method="post" action="">
        <input type="number" name="tinggi" placeholder="Masukkan tinggi piramida">
        <input type="submit" name="submit" value="Buat Piramida">
    </form>

    <?php
    if(isset($_POST['submit'])){
        $tinggi = $_POST['tinggi'];

        // Membuat piramida
        for($i = 1; $i <= $tinggi; $i++){
            // Mencetak spasi sebelum bintang
            for($j = 1; $j <= ($tinggi - $i); $j++){
                echo " &nbsp; ";
            }

            // Mencetak bintang
            for($k = 1; $k <= (2 * $i - 1); $k++){
                echo "*";
            }

            // Pindah baris setelah mencetak bintang
            echo "<br>";
        }
    }
    ?>
</body>
</html>
